#include<stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include<sys/wait.h>
#include<stdlib.h>
#include<string.h>

sig_atomic_t child_exit_status;
void clean_up_child_process (int signal_number) 
{
int status;
wait (&status);
child_exit_status = status;
}

int main (){
	int test =5;
	int test2;
	printf( "------------------------- \n");
	int proces=fork();
	switch (proces)
		{
		case 0:
	{
		printf("proces rodzic\n");
		printf("test %d \n",test);
		test = 1;
		printf("test po modyfikacji %d \n",test);
		test2 = 12;
		break;
	}

		default:	
	{
		printf("proces dziecko\n");
		printf("test %d \n",test);
		test = 1;
		printf("test po modyfikacji %d \n",test);
		test2 = 24;
		break;
	}
		}
	printf("test poza procesem %d \n",test);
	printf("test2 poza procesem  %d \n",test2);

	proces = fork();
	switch(proces)
	{
	default:
	{	
	printf("identyfikator rodzica: %d \n",(int) getpid());
        wait(NULL);
	break;
	}
	case 0:
	{
    	printf("identyfikator dziecka: %d \n",(int) getpid());
   
	exit(0);
	break;
	}}
	struct sigaction sigchld_action;
	memset(&sigchld_action, 0, sizeof (sigchld_action));
	sigchld_action.sa_handler = &clean_up_child_process;
	sigaction(SIGCHLD, &sigchld_action, NULL);
	 
return 0;
}