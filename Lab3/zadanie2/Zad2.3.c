#include <pthread.h>
#include <stdio.h>
struct char_print_parms 
{
	char character;
	int count;
};

void* char_print (void* parameters) 
{
	struct char_print_parms* p = (struct char_print_parms*) parameters;
	int i;
	for (i = 0; i < p->count; ++i)
	fputc (p->character, stderr);
	printf("\n");
	return NULL;
}
int main (int argc, char* argv[]) 

{	
	int ilosc1;
	int ilosc2;		

	pthread_t thread1_id;
	pthread_t thread2_id;

	struct char_print_parms thread1_args;
	struct char_print_parms thread2_args;

	printf("Ile razy wyswietlic znak1? \n");
	scanf("%d", &ilosc1);
	printf("Ile razy wyswietlic znak2? \n");
	scanf("%d", &ilosc2);

	thread1_args.character = *argv[1];
	thread1_args.count = ilosc1;
	pthread_create (&thread1_id, NULL, &char_print, &thread1_args);

	thread2_args.character = *argv[2];
	thread2_args.count = ilosc2;
	pthread_create (&thread2_id, NULL, &char_print, &thread2_args);

	printf("zakonczenie \n");

	pthread_join (thread1_id, NULL);
	pthread_join (thread2_id, NULL);

	return 0;
}// zastosowanie funkcji cenia watkow pozwala nam na zakoczenie dzialania otwarych watkow i kodow w nich zawartych, a dopiero pozniej zakoczenie funkcji main. 